-- mod-version:3
local syntax = require "core.syntax"

syntax.add {
    name = "fatscript",
    files = { "%.fat$" },
    comment = "#",
    -- block_comment = { "/*", "*/" },
    patterns = {
        { pattern = "#.*",                  type = "comment" },
        -- { pattern = { "/%*", "%*/" },        type = "comment" },
        { pattern = { '"', '"', '\\' },     type = "string" },
        { pattern = { "'", "'", '\\' },     type = "string" },
        -- { pattern = "0x%x+",                 type = "number"  },
        { pattern = "%d()%.%.",             type = { "number", 'operator' } },
        { pattern = "%d+[%d%.eE]*f?",       type = "number" },
        -- { pattern = "%.?%d+f?",              type = "number"  },
        { pattern = "<%-()%s[%a%._]+",       type = { "keyword", "string" } }, --import statment
        { pattern = "[%+%-=/%*%^%%<>!~|&]", type = "operator" }, -- passar isso aqui ainda (<-|->|=>|~|@|\\?\\?|\\?|:|\\.\\.<|\\?\\.|\\.|>>)
        -- { pattern = '[\t%s][%?@:][\t%s]',   type = "keyword" },
        { pattern = ":%s()%u%a+",         type = { "normal", "keyword" } },
        { pattern = '%s+[%?@:]%s+',   type = "keyword" },
        { pattern = "[%a_][%w_]*%f[(]",     type = "function" },
        { pattern = "[%a_][%w_]*",          type = "symbol" },
    },
    symbols = { -- i've used literal and keyword2 based on the usagem on js syntax highlight
        ["false"]    = "literal",
        ["null"]     = "literal",
        ["root"]     = "keyword2",
        ["self"]     = "keyword2",
        ["true"]     = "literal",
        ["infinity"] = "literal",
    },
}

